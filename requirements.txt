Django==2.2.1
django-allauth==0.39.1
django-rest-auth==0.9.5
djangorestframework==3.9.4
djangorestframework-jwt==1.11.0
django-cors-headers==3.0.2
imagekitio==1.2.5