from django.conf.urls import url
from .apis import UploadPhotoAPI

urlpatterns = [
    url('(?P<user_type>[a-z-]+)/upload$', UploadPhotoAPI.as_view(), name='user_registration_api'),
]
