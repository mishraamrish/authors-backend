from django.core.validators import URLValidator
from django.conf import settings
from django.db import models

User = settings.AUTH_USER_MODEL


class Comment(models.Model):
    author = models.ForeignKey(User, blank=True, null=True, on_delete=models.PROTECT)
    by_first_name = models.TextField(max_length=20, null=True)
    by_email = models.EmailField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=60, null=False, blank=False)
    reply = models.ManyToManyField('self', symmetrical=False, related_name='comments')


class FeedBack(models.Model):
    by_first_name = models.TextField(max_length=20, null=False, blank=False)
    by_last_name = models.TextField(max_length=20, null=False, blank=False)
    by_email = models.EmailField()
    title = models.TextField(max_length=30, null=False)
    description = models.CharField(max_length=200, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_spam = models.BooleanField(default=False)


class Post(models.Model):
    author = models.OneToOneField(User, related_name='posts', on_delete=models.PROTECT)
    feed_backs = models.ManyToManyField(FeedBack, related_name='feed_back_posts')
    title = models.TextField(max_length=100, null=False)
    quote = models.TextField(max_length=150, null=False)
    description = models.CharField(max_length=2000, null=False)
    is_draft = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    comments = models.ManyToManyField(Comment, related_name='comment_posts')


class Photo(models.Model):
    uploaded_by = models.ForeignKey(User, related_name='photos', on_delete=models.PROTECT)
    photo_name = models.CharField(max_length=100, blank=False, null=False)
    photo_link = models.TextField(validators=[URLValidator()])
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

