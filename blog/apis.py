import base64
import uuid

from django.conf import settings
from rest_framework.generics import CreateAPIView, RetrieveAPIView, ListAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response
from imagekitio.client import Imagekit
from .utils import PostsAPIPagination
from accounts.models import User
from .models import Post, Photo
from .serializers import PostSerializer, PhotoSerializer


class PostListAPI(ListAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    pagination_class = PostsAPIPagination

    def get_queryset(self):
        return self.queryset.order_by('updated_at')


class CreatePostAPI(CreateAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class RetrievePostAPI(RetrieveAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class UpdatePostAPI(UpdateAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_field = 'pk'

    def get_queryset(self):
        user = self.request.user
        if user.role == User.SUPER_USER:
            return self.queryset.filter(pk=self.kwargs.get('pk'))
        else:
            return self.queryset.filter(pk=self.kwargs.get('pk'), author_id=user.id)


class DeletePostAPI(DestroyAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    lookup_field = 'pk'


class UploadPhotoAPI(CreateAPIView):
    serializer_class = PhotoSerializer
    queryset = Photo.objects.all()
    lookup_field = 'pk'

    def post(self, request, *args, **kwargs):
        data = request.FILES
        image_file = data.get("image")
        api_key = settings.IMAGE_KIT_PUBLIC_KEY
        api_secret = settings.IMAGE_KIT_PRIVATE_KEY # required & to be kept secret
        client = Imagekit({"api_key": api_key,
                           "api_secret": api_secret,
                           "imagekit_id": "mishraamrish",
                           "use_secure": True})
        file = base64.b64encode(image_file.read())
        upload_obj = {
            'filename': str(uuid.uuid4()),
            "folder": "/files"
        }
        result = client.upload(file, upload_obj)

        return Response(result)
