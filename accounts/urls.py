from django.conf.urls import url
from .apis import UserRegistrationAPI, UserLoginAPI

urlpatterns = [
    url('(?P<user_type>[a-z-]+)/register$', UserRegistrationAPI.as_view(), name='user_registration_api'),
    url('(?P<user_type>[a-z-]+)/login$', UserLoginAPI.as_view(), name='user_login_api'),
]
