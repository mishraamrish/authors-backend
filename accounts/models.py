from django.contrib.auth.models import AbstractUser, UserManager
from django.core.validators import URLValidator, EmailValidator
from django.utils.translation import gettext_lazy as _
from django.db import models

from blog.models import Photo


class CustomUserManager(UserManager):
    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


class User(AbstractUser):
    SUPER_USER = 1
    AUTHOR = 2
    GUEST_AUTHOR = 3
    ROLE_CHOICES = (
        (SUPER_USER, 'super_user'),
        (AUTHOR, 'author'),
        (GUEST_AUTHOR, 'guest_author'),
    )

    email = models.EmailField(_('email address'), validators=[EmailValidator()], null=False)
    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES)
    profile_picture = models.OneToOneField(Photo, related_name='users', on_delete=models.CASCADE, blank=True, null=True)
    biography = models.CharField(_('biography'), blank=True, max_length=140)
    facebook_url = models.TextField(validators=[URLValidator()])
    linkedin_url = models.TextField(validators=[URLValidator()])
    twitter_url = models.TextField(validators=[URLValidator()])
    confirm_password = None
    objects = CustomUserManager()
    REQUIRED_FIELDS = ['email', 'role']

    class Meta:
        unique_together = ('email', 'role')
