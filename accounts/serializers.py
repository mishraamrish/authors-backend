import uuid
from django.utils.translation import gettext_lazy as _
from django.core.validators import EmailValidator
from django.conf import settings
from rest_framework import serializers
from rest_framework import exceptions
from rest_auth.serializers import LoginSerializer
from django.contrib.auth import get_user_model
from django.db.utils import IntegrityError

User = get_user_model()


class UserRegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[EmailValidator()], required=True)
    password = serializers.CharField(style={'input_type': 'password'}, max_length=128, required=True, write_only=True)
    confirm_password = serializers.CharField(max_length=128, write_only=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password', 'confirm_password')

    @staticmethod
    def get_confirm_password(instance):
        return

    def create(self, data):
        data.pop('confirm_password', None)
        try:
            user = User.objects.create_user(**data)
            return user
        except IntegrityError:
            raise serializers.ValidationError('User with same email already exist')

    def validate(self, attrs):
        confirm_password = attrs.get('confirm_password')
        if attrs.get('password') != confirm_password:
            raise serializers.ValidationError('Password and Confirm Password do not match.')
        return attrs

    def save(self, **kwargs):
        try:
            role = settings.ROLE_MAPPING[self.context.get('view').kwargs.get('user_type')]
        except KeyError:
            raise serializers.ValidationError('Invalid URL, Please check the URL and try again.')
        if role:
            username = str(uuid.uuid4()).replace("-", '')
            self.validated_data['username'] = username
            self.validated_data['role'] = role
            instance = self.create(self.validated_data)
            return instance
        raise serializers.ValidationError('Invalid Request.')


class UserLoginSerializer(LoginSerializer):
    username = None

    def _validate_username_email(self, username, email, password):
        if email and password:
            user = self.authenticate(email=email, role=settings.ROLE_MAPPING[
                     self.context.get('view').kwargs.get('user_type')], password=password)
        elif username and password:
            user = self.authenticate(username=username, password=password)
        else:
            msg = _('Must include either "username" or "email" and "password".')
            raise exceptions.ValidationError(msg)
        return user
