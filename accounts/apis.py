from django.contrib.auth import get_user_model
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateAPIView, DestroyAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_auth.views import LoginView

from .serializers import UserRegistrationSerializer, UserLoginSerializer

User = get_user_model()


class UserRegistrationAPI(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserRegistrationSerializer
    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"message": "Registered Successfully", "data": serializer.data}, status=status.HTTP_201_CREATED)


class UserLoginAPI(LoginView):
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        self.serializer = self.get_serializer(data=request.data,
                                         context={'request': request})
        self.serializer.is_valid(raise_exception=True)
        self.login()
        return self.get_response()


class AuthorListAPI(ListAPIView):
    queryset = User.objects.all()
    serializer_class = ''

    def get_queryset(self):
        return self.queryset.exclude(role=User.SUPER_USER)


class SuperUserListAPI(ListAPIView):
    queryset = User.objects.all()
    serializer_class = ''

    def get_queryset(self):
        return self.queryset.filter(role=User.SUPER_USER)


class AuthorDetailsAPI(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = ''


class SuperUserDetailsAPI(RetrieveUpdateAPIView):
    queryset = User.objects.all()


class DeleteUserAPI(DestroyAPIView):
    queryset = User.objects.all()
    serializer_class = ''
